/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    List<Map.Entry<Integer, Integer>> totalvalueofeverythingsorted;
    //Question 1 - Find Average number of likes per comment.
   public void averageNumOfLikesPerComment() 
    {
        Map<Integer , User> users = DataStore.getInstance().getUsers();
        int likescount=0 ;
        int countcomments = 0; 
        int average = 0;
        for (User user : users.values())
        {
            for (Comment c : user.getComments())
            {
                countcomments++;
                likescount  = likescount + c.getLikes();
           }
        }
        System.out.println("Total number of likes: "+likescount);
        System.out.println("Total number of comments: "+countcomments);
        average = likescount/countcomments;
        System.out.println("Average number of likes per comment:" +average);
        System.out.println();
    } 
   //Question 2 - Find the post with most liked comments.  - Approach 2
   public void postWithMostLikedComments(){
        Map<Integer, Integer> postCommentLikesCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        int likes = 0;
         for (Post post : posts.values())
        {
            likes = 0;
            for (Comment c : post.getComments())
            {
                if(postCommentLikesCount.containsKey(post.getPostId()))
                {
                    likes = postCommentLikesCount.get(post.getPostId());
                    likes += c.getLikes();
                }
                else{
                likes += c.getLikes();
                }
                postCommentLikesCount.put(post.getPostId(), likes);
            }
        }
        System.out.println("All the posts with their like count"+postCommentLikesCount);
        int max = 0;
        int maxId = 0;
        for(int id : postCommentLikesCount.keySet())
        {
            if(postCommentLikesCount.get(id)> max)
            {
                max = postCommentLikesCount.get(id);
                maxId = id;
            }
        }
         System.out.println("Post Id: " + maxId+" has most likes: "+max);
         System.out.println();
    }
   
  //Question 2 - Find the post with most liked comments.  - Approach 2
   
   public void getFiveComments(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>() {
            
            
            public int compare(Comment c1, Comment c2) {
                return Double.compare(c2.getLikes(),c1.getLikes());
                //c2.getLike()-c1.getLikes();
            }
        });
  for (int i =0; i <5&&i<comments.size();i++){
      System.out.println(commentList.get(i));
  }
        
    }
   
   //Question 3
    public void mostComments(){
        
       
        int temp=0;
        
Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        //System.out.println(posts);
     List<Post> postList = new ArrayList<>(posts.values());
     

        Collections.sort(postList, new Comparator<Post>() {
           
            
            @Override
            public int compare(Post c1, Post c2) {
                return Double.compare(c2.getComments().size(),c1.getComments().size());
                //c2.getLike()-c1.getLikes();
            }
        });
    for(int i=0; i<posts.size()&&i<5;i++){
        System.out.println(postList.get(i));
    }
    
    }
   
    //Question 4 - Top 5 inactive users based on post
     public void inactiveUsers(){

Map<Integer, Post> post = DataStore.getInstance().getPosts();
List<Post> postList = new ArrayList<>(post.values());

Map<Integer, Integer> temp = new HashMap();
int count =0;
       for(Post posti: post.values()){
           
           if(temp.containsKey(posti.getUserId())){
   count = temp.get(posti.getUserId());
          
         ++count;

           }
           else{
               count =1;

           }
           temp.put(posti.getUserId(),count);
    
       }

   
       
       List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(
        temp.entrySet());
       
       Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
 Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() { 
   
     @Override
    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
        return o1.getValue().compareTo(o2.getValue());
    }
});
 
 for(int i=0;i<list.size()&&i<5;i++){
     System.out.println("userId: "+list.get(i).getKey()+","+"Number of posts: "+list.get(i).getValue());
 }
 
 
 
 
        
    }
   
   //Question 5 - Top 5 inactive users based on total comments they created.
   public void inactiveUsersBasedOnCommentsCreated(){
       Map<Integer, Integer> numOfCommentsPerUser = new HashMap<>(); 
       Map<Integer , User> users = DataStore.getInstance().getUsers();
       int usercommentcount = 0;
       for(User user: users.values() ){
           usercommentcount = 0;
           for(Comment c : user.getComments()){
               //if( numOfCommentsPerUser.containsKey(c.getUserId()))
               //System.out.println(c);
               usercommentcount++;
               numOfCommentsPerUser.put(c.getUserId(),usercommentcount);
           }
//           System.out.println(usercommentcount);
//           System.out.println(numOfCommentsPerUser);   
       }
       List<Map.Entry<Integer, Integer>> sortedNumOfCommentsPerUser = new ArrayList<Map.Entry<Integer, Integer>>(numOfCommentsPerUser.entrySet());
       Collections.sort(sortedNumOfCommentsPerUser, new Comparator<Map.Entry<Integer, Integer>>() {
          @Override
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o1.getValue().compareTo(o2.getValue());
          }
       });
       System.out.println("Top 5 inactive users based on comments are shown below:");
        Iterator it = sortedNumOfCommentsPerUser.listIterator();
        int n = 0;
        while(it.hasNext() && n < 5){
           Map.Entry keyValuePair = (Map.Entry)it.next();
           System.out.println("UserId: " +keyValuePair.getKey()+ " has number of comments = " +keyValuePair.getValue());
           n++;
    }
    }
   
   //Question 6 : Top 5 inactive users overall
   
   public void  inactiveUsersoverall()
{
    Map<Integer,Integer> inactiveUsersoverall = new HashMap<>();
    Map<Integer , User> users = DataStore.getInstance().getUsers();
    Map<Integer , Post> posts = DataStore.getInstance().getPosts();
    Map<Integer,Integer> totalvalueofeverything = new HashMap<>();
    int commentsbyid=0;
    int likes = 0;
    int count=0;
    int totalvalue=0;
    for(User user : users.values())
    {   
        commentsbyid=0;
        //System.out.println(user);
        commentsbyid = user.getComments().size();
        //System.out.println("comments by id  "+user.getId()+" :"+commentsbyid);
        
        likes = 0;
        for(Comment c : user.getComments())
        {
            likes = 0;
            //System.out.println(c);
            if(inactiveUsersoverall.containsKey(user.getId()))
                {
                    likes = inactiveUsersoverall.get(user.getId());
                    likes += c.getLikes();
                    
                }
                else{
                likes += c.getLikes();
                
                }
               inactiveUsersoverall.put(user.getId(), likes);
                
            
        }//System.out.println("Likes by user :"+likes);
           
       
        Map<Integer, Integer> temp = new HashMap();
        for(Post post : posts.values())
        {
            
            if(temp.containsKey(post.getUserId()))
            {
                count = temp.get(post.getUserId());
                ++count;
                
            }
            else{
                count=1;
            }
            temp.put(post.getUserId(), count);
            
        }//System.out.println("Count of post : "+count);
        
        
        
        totalvalue = commentsbyid + likes + count;
    
    //System.out.println("Total number of activities by user : "+totalvalue);
   
   

        totalvalueofeverything.put(user.getId(),totalvalue);
           

}
    //System.out.println(totalvalueofeverything); 
    totalvalueofeverythingsorted = new ArrayList<Map.Entry<Integer, Integer>>(totalvalueofeverything.entrySet());
       Collections.sort(totalvalueofeverythingsorted, new Comparator<Map.Entry<Integer, Integer>>() {
          @Override
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o1.getValue().compareTo(o2.getValue());
          }
       });
       System.out.println("Top 5 inactive users overall are shown below:");
        Iterator it = totalvalueofeverythingsorted.listIterator();
        int n = 0;
        while(it.hasNext() && n < 5){
           Map.Entry keyValuePair = (Map.Entry)it.next();
           System.out.println("UserId: " +keyValuePair.getKey()+ " has total activities  = " +keyValuePair.getValue());
           n++;
    }  
        
        totalvalueofeverythingsorted = new ArrayList<Map.Entry<Integer, Integer>>(totalvalueofeverything.entrySet());
       Collections.sort(totalvalueofeverythingsorted, new Comparator<Map.Entry<Integer, Integer>>() {
          @Override
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o2.getValue().compareTo(o1.getValue());
          }
       });
       System.out.println("Top 5 proactive users overall are shown below:");
        Iterator it1 = totalvalueofeverythingsorted.listIterator();
        int n1 = 0;
        while(it1.hasNext() && n1 < 5){
           Map.Entry keyValuePair = (Map.Entry)it1.next();
           System.out.println("UserId: " +keyValuePair.getKey()+ " has total activities  = " +keyValuePair.getValue());
           n1++;
    }
        
} 

}
